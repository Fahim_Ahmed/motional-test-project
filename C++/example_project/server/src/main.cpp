#include "PullSocket.hpp"
#include <iostream>

#include "LogManager.hpp"
#include "ExternalConfigHandler.hpp"

using namespace example;

int main() {


    // Load configs from external file. -----------
    ExternalConfigHandler ex("./config.ini");
    std::string serverUrl = ex.GetServerURL();
    std::string serverPort = ex.GetPort();

    int printLogs = ex.GetPrintOnConsole();
    int writeOnDisk = ex.GetWriteOnDisk();
    int writeReadableLogs = ex.GetWriteLogOnDisk();
    std::string logDirPath = ex.GetLogDirPath();


    // Launch server -----------
    std::string url = serverUrl + ":" + serverPort;
    PullSocket receiver(30000);
    receiver.ListenForConnections(url);

    std::cout << "Listening on " << url << std::endl;


    LogManager logManager(logDirPath);

    // Example: log read from storage with specified date and uid. -----------
    // logManager.PrintLogsOf("GO_1234", 1602491357505);         // UTC format
    // logManager.PrintLogsOf("GO_1234", "2020", "Oct", "12");   // Year, month, day
    // ---------------------------------

    for (;;) {
        std::vector<uint8_t> buffer_pointer;

        size_t receivedBytes;
        bool success = receiver.Receive(buffer_pointer, receivedBytes);

        if (success) {
            if(writeOnDisk == 1) logManager.WriteToDisk(buffer_pointer, receivedBytes);
            if(printLogs == 1) logManager.PrintLogs(&buffer_pointer[0]);
            if(writeReadableLogs == 1) logManager.WriteReadableLogsToFile(&buffer_pointer[0]);
        }
    }

    return 0;
}
