#include <iostream>

namespace example {

	template <typename T> class FBObjectParser {

	public:

		const T* dataBundle;

		virtual std::string GetUID() {
			if (!isEmpty()) {
				return dataBundle->data()->Get(0)->uid()->c_str();
			}

			return "";
		}

		virtual bool isEmpty() {
			return dataBundle->data()->size() == 0;
		}
	};
}