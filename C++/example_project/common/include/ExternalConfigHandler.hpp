#include <iostream>
#include <Windows.h>

namespace example {

	class ExternalConfigHandler {
	
	public:
		ExternalConfigHandler(std::string configFilePath) {
			this->configFilePath = configFilePath;
		}

		std::string GetServerURL() {
			LPCSTR path = configFilePath.c_str();
			TCHAR protocolChar[32];

			GetPrivateProfileString("network configs", "server_url", "tcp://127.0.0.1", protocolChar, 20, path);
			GetLastErrorAsString();

			return protocolChar;
		}

		std::string GetPort() {
			LPCSTR path = configFilePath.c_str();
			TCHAR protocolChar[32];

			GetPrivateProfileString("network configs", "server_port", "6120", protocolChar, 6, path);
			GetLastErrorAsString();

			return protocolChar;
		}

		std::string GetLogDirPath() {
			LPCSTR path = configFilePath.c_str();
			TCHAR protocolChar[128];

			GetPrivateProfileString("log configs", "log_dir_path", "LOGS", protocolChar, 128, path);
			GetLastErrorAsString();

			return protocolChar;
		}

		int GetWriteOnDisk() {
			LPCSTR path = configFilePath.c_str();
			TCHAR protocolChar[32];

			UINT val = GetPrivateProfileInt("log configs", "write_to_disk", 1, path);
			GetLastErrorAsString();

			return val;
		}
		
		int GetWriteLogOnDisk() {
			LPCSTR path = configFilePath.c_str();
			TCHAR protocolChar[32];

			UINT val = GetPrivateProfileInt("log configs", "write_logs_to_disk", 1, path);
			GetLastErrorAsString();

			return val;
		}

		int GetPrintOnConsole() {
			LPCSTR path = configFilePath.c_str();

			UINT val = GetPrivateProfileInt("log configs", "print_on_console", 1, path);
			GetLastErrorAsString();

			return val;
		}



	private:
		std::string configFilePath;

		void GetLastErrorAsString() {
			//Get the error message, if any.
			DWORD errorMessageID = ::GetLastError();
			if (errorMessageID == 0)
				return; // std::string(); //No error message has been recorded

			LPSTR messageBuffer = nullptr;
			size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

			std::string message(messageBuffer, size);

			//Free the buffer.
			LocalFree(messageBuffer);

			std::cout << "Error: " << message << std::endl;
		}
	
	};
}