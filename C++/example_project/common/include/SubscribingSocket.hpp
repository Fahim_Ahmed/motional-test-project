#ifndef SIMULATIONS_ASSIGNMENT_SUBSCRIBINGSOCKET_H
#define SIMULATIONS_ASSIGNMENT_SUBSCRIBINGSOCKET_H
#include <nng/nng.h>
#include <string>
#include <vector>
namespace example
{
    class SubscribingSocket
    {
    public:
        SubscribingSocket(const size_t bufferSize);
        ~SubscribingSocket();
        SubscribingSocket(SubscribingSocket&& rhs) noexcept;
        SubscribingSocket& operator=(SubscribingSocket&& rhs) noexcept;
        SubscribingSocket(const SubscribingSocket& rhs) = delete;
        SubscribingSocket& operator=(SubscribingSocket& rhs) = delete;

        void DialToServer(const std::string& url);
        bool PollForRecievedData(std::vector<uint8_t>& data);

    private:
        nng_socket m_Socket{};
        std::vector<uint8_t> m_MessageBuffer;
    };
}
#endif //SIMULATIONS_ASSIGNMENT_SUBSCRIBINGSOCKET_H
