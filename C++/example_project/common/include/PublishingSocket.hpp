#ifndef SIMULATIONS_ASSIGNMENT_PUBLISHINGSOCKET_HPP
#define SIMULATIONS_ASSIGNMENT_PUBLISHINGSOCKET_HPP
#include <nng/nng.h>
#include <string>
namespace example
{
    class PublishingSocket
    {
    public:
        PublishingSocket();
        ~PublishingSocket();
        PublishingSocket(PublishingSocket&& rhs) noexcept;
        PublishingSocket& operator=(PublishingSocket&& rhs) noexcept;
        PublishingSocket(const PublishingSocket& rhs) = delete;
        PublishingSocket& operator=(PublishingSocket& rhs) = delete;

        void ListenForConnections(const std::string& url);
        void Send(uint8_t* dataPointer, size_t dataSize);

    private:
        nng_socket m_Socket{};
    };
}
#endif //SIMULATIONS_ASSIGNMENT_PUBLISHINGSOCKET_HPP
