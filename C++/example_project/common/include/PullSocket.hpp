#include <nng/nng.h>
#include <string>
#include <vector>

namespace example {

	// NNG pipeline: Pull

	class PullSocket {
	
	public:
		PullSocket(const size_t bufferSize);
		~PullSocket();

		int ListenForConnections(const std::string& url);

		bool Receive(std::vector<uint8_t>& data, size_t& sz);

		void Free(void* buf, size_t sz);

	private:
		nng_socket m_Socket{};
		std::vector<uint8_t> m_MessageBuffer;

		void fatal(const char* func, int rv);
	};

}