#include <nng/nng.h>
#include <string>

namespace example {

	// NNG pipeline: Push 

	class PushSocket {

	public:
		PushSocket();
		~PushSocket();

		void Dial(const std::string& url);
		void SendMessage(char* msg);
		void PushSocket::CloseSocket();

	private:
		nng_socket m_Socket{};

		void fatal(const char* func, int rv);

	};
}