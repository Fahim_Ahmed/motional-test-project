#include <ctime>
#include <iostream>
#include <sstream>
#include <iomanip>

namespace example {

    // A utility class to parse UTC timestamp

    class TimeUtils {
    
    public:
        static std::string ParseUtcTimestamp(double utc) {

            auto ms = std::fmod(utc, 1000);

            std::time_t now = utc / 1000.0;
            std::tm* now_tm = std::localtime(&now);

            char buf[42];
            std::strftime(buf, 42, "%Y-%b-%d %H:%M:%S.", now_tm);

            std::stringstream ss;
            ss << buf << std::setfill('0') << std::setw(3) << ms;

            return ss.str();
        }

        static std::string GetYearFromUTC(double utc) {

            std::time_t now = utc / 1000;
            std::tm* now_tm = std::localtime(&now);

            char buf[42];
            std::strftime(buf, 42, "%Y", now_tm);

            return buf;
        }

        static std::string GetMonthFromUTC(double utc) {

            std::time_t now = utc / 1000;
            std::tm* now_tm = std::localtime(&now);

            char buf[42];
            std::strftime(buf, 42, "%b", now_tm);

            return buf;
        }

        static std::string GetDayFromUTC(double utc) {

            std::time_t now = utc / 1000;
            std::tm* now_tm = std::localtime(&now);

            char buf[42];
            std::strftime(buf, 42, "%d", now_tm);

            return buf;
        }

        static std::string GetTimeFromUTC(double utc) {

            std::time_t now = utc / 1000;
            std::tm* now_tm = std::localtime(&now);

            char buf[42];
            std::strftime(buf, 42, "%H-%M-%S", now_tm);

            auto ms = std::fmod(utc, 1000);
            std::stringstream ss;
            ss << buf << "." << ms;

            return ss.str();
        }

    };
}