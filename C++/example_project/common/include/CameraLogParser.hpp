#include <vector>
#include "CameraLogSchema_generated.h"
#include "FBObjectParser.hpp"

using namespace Modules::NetworkPacket;

namespace example {

	// A flatbuffers object parser derived from generic FBObjectParser.

	class CameraLogParser : public FBObjectParser<DataBundle> {

	public:
		CameraLogParser(const void* buf);

		std::vector<double> GetTimestamps();
		std::vector<const Vec3*> GetPositions();
		std::vector<const Vec4*> GetRotations();
		std::vector<const flatbuffers::Vector<int8_t>*> GetActions();

		void PrintTimestamps();
		void PrintPositions();
		void PrintRotations();
		void PrintActions();

	private:
		std::vector<uint8_t> buffer_pointer;
	};
}