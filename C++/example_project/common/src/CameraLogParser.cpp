#include "CameraLogParser.hpp"
#include "TimeUtils.hpp"

namespace example {

	CameraLogParser::CameraLogParser(const void* buf) {

		this->dataBundle = GetDataBundle(buf);
	}

	std::vector<double> CameraLogParser::GetTimestamps() {
		std::vector<double> timestamps;

		int len = dataBundle->data()->size();

		for (int i = 0; i < len; i++) {
			timestamps.push_back(dataBundle->data()->Get(i)->timestamp());
		}

		return timestamps;
	}

	std::vector<const Vec3*> CameraLogParser::GetPositions() {
		std::vector<const Vec3*> positions;

		int len = dataBundle->data()->size();

		for (int i = 0; i < len; i++) {
			positions.push_back(dataBundle->data()->Get(i)->position());
		}

		return positions;
	}

	std::vector<const Vec4*> CameraLogParser::GetRotations() {
		std::vector<const Vec4*> rotations;

		int len = dataBundle->data()->size();

		for (int i = 0; i < len; i++) {
			rotations.push_back(dataBundle->data()->Get(i)->rotation());
		}

		return rotations;
	}

	std::vector<const flatbuffers::Vector<int8_t>*> CameraLogParser::GetActions() {
		std::vector<const flatbuffers::Vector<int8_t>*> actions;

		int len = dataBundle->data()->size();

		for (int i = 0; i < len; i++) {
			actions.push_back(dataBundle->data()->Get(i)->action_taken());
		}

		return actions;
	}

	void CameraLogParser::PrintTimestamps() {
		int len = dataBundle->data()->size();

		for (int i = 0; i < len; i++) {
			std::cout << GetUID() << ": " << example::TimeUtils::ParseUtcTimestamp(dataBundle->data()->Get(i)->timestamp()) << std::endl;
		}
	}

	void CameraLogParser::PrintPositions() {
		int len = dataBundle->data()->size();

		for (int i = 0; i < len; i++) {
			std::cout << GetUID() << ": ";

			auto p = dataBundle->data()->Get(i)->position();
			std::cout << "X: " << std::setw(8) << p->x() << " ";
			std::cout << "Y: " << std::setw(8) << p->y() << " ";
			std::cout << "Z: " << std::setw(8) << p->z() << " " << std::endl;
		}
	}

	void CameraLogParser::PrintRotations() {
		int len = dataBundle->data()->size();

		for (int i = 0; i < len; i++) {
			std::cout << GetUID() << ": ";

			auto r = dataBundle->data()->Get(i)->rotation();
			std::cout << "X: " << std::setw(8) << r->x() << " ";
			std::cout << "Y: " << std::setw(8) << r->y() << " ";
			std::cout << "Z: " << std::setw(8) << r->z() << " ";
			std::cout << "W: " << std::setw(8) << r->w() << " " << std::endl;
		}
	}

	void CameraLogParser::PrintActions() {
		int len = dataBundle->data()->size();

		for (int i = 0; i < len; i++) {
			auto action = dataBundle->data()->Get(i)->action_taken();

			std::cout << GetUID() << ": ";
			if (action->size() == 0) {
				std::cout << "NONE" << std::endl;
				continue;
			}

			for (size_t j = 0; j < action->size(); j++) {
				if (action->Get(j) == 0) { std::cout << "ZOOM"; }
				else if (action->Get(j) == 1) { std::cout << "PAN"; }
				else if (action->Get(j) == 2) { std::cout << "ROTATE"; }

				std::cout << " ";
			}

			std::cout << std::endl;
		}
	}
}