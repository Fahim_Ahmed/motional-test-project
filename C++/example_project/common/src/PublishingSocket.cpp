#include "PublishingSocket.hpp"
#include <nng/protocol/pubsub0/pub.h>
#include <chrono>
#include <thread>
namespace example
{
    PublishingSocket::PublishingSocket()
    {
        nng_pub0_open(&m_Socket);
    }

    PublishingSocket::~PublishingSocket()
    {
        using namespace std::chrono_literals;
        auto startTime = std::chrono::steady_clock::now();
        while ((std::chrono::steady_clock::now() - startTime) < 3s)
        {
            if (nng_close(m_Socket) != EINTR)
            {
                return;
            }
            std::this_thread::sleep_for(20ms);
        }
    }

    PublishingSocket::PublishingSocket(PublishingSocket&& rhs) noexcept
    {
        if (&rhs != this)
        {
            (*this) = std::move(rhs);
        }
    }
    PublishingSocket& PublishingSocket::operator=(PublishingSocket&& rhs) noexcept
    {
        if(&rhs != this)
        {
            std::swap(m_Socket, rhs.m_Socket);
        }
        return *this;
    }


    void PublishingSocket::ListenForConnections(const std::string& url)
    {
        nng_listen(m_Socket, url.c_str(), nullptr, 0);
    }



    void PublishingSocket::Send(uint8_t* dataPointer, size_t dataSize)
    {
        if (dataPointer != nullptr && dataSize != 0)
        {
            nng_send(m_Socket, dataPointer, dataSize, 0);
        }
    }
}