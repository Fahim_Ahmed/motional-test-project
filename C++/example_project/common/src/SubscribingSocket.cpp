#include "SubscribingSocket.hpp"
#include <nng/protocol/pubsub0/sub.h>
#include <chrono>
#include <thread>
#include <sstream>
namespace example
{
    namespace
    {
        const std::chrono::milliseconds ReconnectionMinTime{10};
        const std::chrono::milliseconds ReconnectionMaxTime{500};
    }

    SubscribingSocket::SubscribingSocket(const size_t messageBufferSize)
    :m_MessageBuffer(messageBufferSize)
    {
        nng_sub0_open(&m_Socket);
        nng_setopt_size(m_Socket, NNG_OPT_RECVMAXSZ, 0); // unbounded max recv size
        nng_setopt_ms(m_Socket, NNG_OPT_RECONNMINT, std::chrono::duration_cast<std::chrono::milliseconds>(ReconnectionMinTime).count());
        nng_setopt_ms(m_Socket, NNG_OPT_RECONNMAXT, std::chrono::duration_cast<std::chrono::milliseconds>(ReconnectionMaxTime).count());
    }

    SubscribingSocket::~SubscribingSocket()
    {
        using namespace std::chrono_literals;
        auto startTime = std::chrono::steady_clock::now();
        while ((std::chrono::steady_clock::now() - startTime) < 3s)
        {
            if (nng_close(m_Socket) != EINTR)
            {
                return;
            }
            std::this_thread::sleep_for(20ms);
        }
    }

    SubscribingSocket::SubscribingSocket(SubscribingSocket&& rhs) noexcept
    {
        if (&rhs != this)
        {
            std::swap(m_MessageBuffer,rhs.m_MessageBuffer);
            std::swap(m_Socket, rhs.m_Socket);
        }
    }

    SubscribingSocket& SubscribingSocket::operator=(SubscribingSocket&& rhs) noexcept
    {
        if(&rhs != this)
        {
            std::swap(m_MessageBuffer,rhs.m_MessageBuffer);
            std::swap(m_Socket, rhs.m_Socket);
        }
        return *this;
    }

    void SubscribingSocket::DialToServer(const std::string& url)
    {
        auto err = nng_dial(m_Socket, url.c_str(), nullptr, NNG_FLAG_NONBLOCK);
        if(err)
        {
            std::stringstream ss;
            ss<<"subscribing socket unable to connect to server : "<<err;
            throw std::runtime_error(ss.str());
            return;
        }
        nng_setopt(m_Socket, NNG_OPT_SUB_SUBSCRIBE, "", 0);
    }

    bool SubscribingSocket::PollForRecievedData(std::vector<uint8_t>& data)
    {
        size_t receivedBytes = m_MessageBuffer.size();
        auto err = nng_recv(m_Socket, m_MessageBuffer.data(), &receivedBytes, NNG_FLAG_NONBLOCK);
        if (!err)
        {
            if (receivedBytes > m_MessageBuffer.size())
            {
                std::stringstream ss;
                ss << "SubscribingSocket::Recv PollForRecievedData length exceeded: buffer size:" << m_MessageBuffer.size();
                throw std::runtime_error(ss.str());
                return false;
            }
            data.resize(receivedBytes);
            auto end_iterator = m_MessageBuffer.begin();
            std::advance(end_iterator, receivedBytes);
            std::copy(m_MessageBuffer.begin(), end_iterator, data.begin());

            return true;
        }
        return false;
    }

}