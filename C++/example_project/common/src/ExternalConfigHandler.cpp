#include <Windows.h>
#include "ExternalConfigHandler.hpp"

namespace example {

	ExternalConfigHandler::ExternalConfigHandler(std::string configFilePath) {

		this->configFilePath = configFilePath;

	}

	void ExternalConfigHandler::LoadConfigs() {
		LPCSTR path = configFilePath.c_str();
		LPSTR val;

		GetPrivateProfileString("camera controller configs", "move_speed", "2", val, 10, path);
		std::cout << val << " External configs loaded." << std::endl;
	}
}