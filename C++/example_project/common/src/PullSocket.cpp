#include "PullSocket.hpp"
#include <chrono>
#include <thread>
#include <nng/protocol/pipeline0/pull.h>
#include <sstream>

namespace example {
	PullSocket::PullSocket(const size_t messageBufferSize) : m_MessageBuffer(messageBufferSize) {
        int rv;

        if ((rv = nng_pull0_open(&m_Socket)) != 0) {
            fatal("nng_pull0_open", rv);
            return;
        }

        printf("Socket opened.\n");
	}

	PullSocket::~PullSocket() {
        using namespace std::chrono_literals;
        auto startTime = std::chrono::steady_clock::now();

        while ((std::chrono::steady_clock::now() - startTime) < 3s) {
            if (nng_close(m_Socket) != EINTR){
                return;
            }
            std::this_thread::sleep_for(20ms);
        }
	}

    int PullSocket::ListenForConnections(const std::string& url) {
        int rv;

        if ((rv = nng_listen(m_Socket, url.c_str(), NULL, 0)) != 0) {
            fatal("nng_listen", rv);
        }

        return rv;
    }

        //uint8_t* PullSocket::Receive(size_t *sz) {
    //    int rv;
    //    uint8_t* buf = NULL;
    //    if ((rv = nng_recv(m_Socket, &buf, sz, NNG_FLAG_ALLOC)) != 0) {
    //        fatal("nng_recv", rv);
    //        return NULL;
    //    }
    //    //printf("NODE0: RECEIVED \"%s\"\n", );
    //    //nng_free(buf, sz);

    //    return buf;
    //}

    /*void PullSocket::Receive() {
        char* buf = NULL;
        int rv;
        size_t receivedBytes = m_MessageBuffer.size();
        
        if ((rv = nng_recv(m_Socket, &buf, &receivedBytes, NNG_FLAG_ALLOC)) != 0) {
            fatal("nng_recv", rv);
            return;
        }

        if (receivedBytes > m_MessageBuffer.size()) {
            std::stringstream ss;
            ss << "Data length exceeded: buffer size:" << m_MessageBuffer.size();
            throw std::runtime_error(ss.str());
            return;
        }
        
        printf("NODE0: RECEIVED \"%s\"\n", buf);
        
        nng_free(buf, receivedBytes);
    }*/

    bool PullSocket::Receive(std::vector<uint8_t>& data, size_t &sz) {
        size_t receivedBytes = m_MessageBuffer.size();

        auto err = nng_recv(m_Socket, m_MessageBuffer.data(), &receivedBytes, NNG_FLAG_NONBLOCK);
        if (!err)
        {

            //printf("received: %d\n", receivedBytes);
            sz = receivedBytes;

            if (receivedBytes > m_MessageBuffer.size())
            {
                std::stringstream ss;
                ss << "PullSocket::Recv length " << receivedBytes << " exceeded: buffer size:" << m_MessageBuffer.size() << "\n";
                //throw std::runtime_error(ss.str());
                printf("%s", ss.str().c_str());
                return false;
            }

            data.resize(receivedBytes);
            auto end_iterator = m_MessageBuffer.begin();
            std::advance(end_iterator, receivedBytes);
            std::copy(m_MessageBuffer.begin(), end_iterator, data.begin());

            //nng_free(&m_MessageBuffer, receivedBytes);

            return true;
        }

        return false;
    }

    //bool PullSocket::Receive(uint8_t* buf) {
    //    //uint8_t* buf = NULL;
    //    int rv;
    //    size_t receivedBytes; // = buf.size();

    //    if ((rv = nng_recv(m_Socket, &buf, &receivedBytes, NNG_FLAG_ALLOC)) != 0) {
    //        fatal("nng_recv", rv);
    //        return false;
    //    }

    //    /*if (receivedBytes > m_MessageBuffer.size()) {
    //        std::stringstream ss;
    //        ss << "Data length exceeded: buffer size:" << m_MessageBuffer.size();
    //        throw std::runtime_error(ss.str());
    //        return false;
    //    }*/

    //    return true;
    //}

    void PullSocket::Free(void * buf, size_t sz) {
        nng_free(buf, sz);
    }

    void PullSocket::fatal(const char* func, int rv) {
        fprintf(stderr, "%s: %s\n", func, nng_strerror(rv));
        //exit(1);
    }
}