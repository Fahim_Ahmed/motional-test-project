#include "PushSocket.hpp"
#include <chrono>
#include <thread>
#include <nng/protocol/pipeline0/push.h>

namespace example {
	PushSocket::PushSocket()
	{
		int rv;
		int bytes;

		if ((rv = nng_push0_open(&m_Socket)) != 0) {
			fatal("nng_push0_open", rv);
			return;
		}

		printf("Socket opened successfully.\n");
	}

	PushSocket::~PushSocket()
	{
		using namespace std::chrono_literals;
		auto startTime = std::chrono::steady_clock::now();
		while ((std::chrono::steady_clock::now() - startTime) < 3s)
		{
			if (nng_close(m_Socket) != EINTR)
			{
				return;
			}
			std::this_thread::sleep_for(20ms);
		}
	}

	void PushSocket::Dial(const std::string& url) {
		int rv;
		if ((rv = nng_dial(m_Socket, url.c_str(), NULL, 0)) != 0) {
			fatal("nng_dial", rv);
			return;
		}
	}

	// Send
	void PushSocket::SendMessage(char* msg) {
		int rv;
		if ((rv = nng_send(m_Socket, msg, strlen(msg) + 1, 0)) != 0) {
			fatal("nng_send", rv);
			return;
		}
	}

	// Close
	void PushSocket::CloseSocket() {
		nng_close(m_Socket);
		printf("Socket closing.\n");
	}

	void PushSocket::fatal(const char* func, int rv) {
		fprintf(stderr, "%s: %s\n", func, nng_strerror(rv));
		exit(1);
	}
}

