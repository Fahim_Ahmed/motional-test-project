#include "LogManager.hpp"
#include "LogReader.hpp"

namespace example {
	LogManager::LogManager(std::string logDir) {
		this->logDir = logDir;
		fs::create_directory(logDir);
	}

	void LogManager::WriteToDisk(std::vector<uint8_t> buf, size_t receivedBytes) {
		auto th = std::async(std::launch::async, &LogManager::WriteJob, this, buf, receivedBytes);
	}

	void LogManager::WriteJob(std::vector<uint8_t> &buf, size_t sz) {
		CameraLogWriter writer(logDir);
		auto d = GetDataBundle(&buf[0]);

		try {
			writer.WriteToFile(d, sz);
		}
		catch (const char* msg) {
			std::cout << msg << std::endl;
		}
	}

	//void LogManager::PrintLogs(std::vector<uint8_t> &buf) {
	void LogManager::PrintLogs(const void *buf) {
		CameraLogParser clp(buf);
		auto logs = GetReadableLog(&clp);
		fmt::print(logs);
	}

	void LogManager::WriteReadableLogsToFile(const void* buf) {
		CameraLogParser clp(buf);		
		auto path = logDir + "/" + clp.GetUID() + ".log";
		
		std::ofstream ofs(path.c_str(), std::ios_base::out | std::ios_base::app);
		ofs << lastLogs;
		ofs.close();
	}

	std::string LogManager::GetReadableLog(CameraLogParser *clp) {

		auto uid = clp->GetUID();
		auto t = clp->GetTimestamps();
		auto p = clp->GetPositions();
		auto r = clp->GetRotations();
		auto ac = clp->GetActions();

		int l = p.size();

		std::stringstream ss;

		for (int i = 0; i < l; i++) {
			ss << uid << ": ";
			ss << TimeUtils::ParseUtcTimestamp(t[i]);

			ss << fmt::format(" | Pos: {:0.4f} {:0.4f} {:0.4f}", p[i]->x(), p[i]->y(), p[i]->z());
			ss << fmt::format(" | Rot: {:0.4f} {:0.4f} {:0.4f} {:0.4f}", r[i]->x(), r[i]->y(), r[i]->z(), r[i]->w());

			// actions
			ss << " | ";

			auto action = ac[i];
			if (action->size() == 0) {
				ss << "NONE" << std::endl;
				continue;
			}

			for (size_t j = 0; j < action->size(); j++) {
				if (action->Get(j) == 0) { ss << "ZOOM"; }
				else if (action->Get(j) == 1) { ss << "PAN"; }
				else if (action->Get(j) == 2) { ss << "ROTATE"; }

				ss << " ";
			}

			ss << std::endl;
		}

		lastLogs = ss.str();

		return lastLogs;
	}

	void LogManager::PrintLogsOf(std::string uid, double utc) {
		if (logDir == "") std::cout << "Log directory not found." << std::endl;
		LogReader reader(logDir);

		try {
			auto logs = reader.GetAllLogsOfUid(uid, utc);
			for (auto l : logs) {
				PrintLogs(&l[0]);
			}
		}
		catch (const char* ex)
		{
			std::cout << ex << std::endl;
		}
	}

	void LogManager::PrintLogsOf(std::string uid, std::string year, std::string mon, std::string day) {
		if (logDir == "") std::cout << "Log directory not found." << std::endl;
		LogReader reader(logDir);

		try {
			auto logs = reader.GetAllLogsOfUid(uid, year, mon, day);
			for (auto l : logs) {
				PrintLogs(&l[0]);
			}
		}
		catch (const char* ex)
		{
			std::cout << ex << std::endl;
		}
	}
}