#include <iostream>
#include <fstream>
#include <filesystem>
#include <iterator>
#include <vector>
#include <future>

#include "TimeUtils.hpp"

namespace fs = std::filesystem;

namespace example {

	// Reads flatbuffers binary from storage

	class LogReader {

	public:

		LogReader() {}
		LogReader(std::string logDirPath) { this->logDirPath = logDirPath; }

		void Read(std::string filePath, std::vector<char> &buffer) {
			std::ifstream infile(filePath, std::ios_base::binary);

			if (infile.fail()) {
				std::cout << "File not found." << std::endl;
				return;
			}

			buffer = std::vector<char>((std::istreambuf_iterator<char>(infile)),
				std::istreambuf_iterator<char>());
			infile.close();
		}

		std::vector<std::vector<char>> GetAllLogsOfUid(std::string uid, double ofDate) {

			std::string year = example::TimeUtils::GetYearFromUTC(ofDate);
			std::string month = example::TimeUtils::GetMonthFromUTC(ofDate);
			std::string day = example::TimeUtils::GetDayFromUTC(ofDate);

			return GetAllLogsOfUid(uid, year, month, day);
		}

		std::vector<std::vector<char>> GetAllLogsOfUid(std::string uid, std::string year, std::string month, std::string day) {
			std::vector<std::vector<char>> logs;

			std::string path = logDirPath + "/" + uid + "/" + year + "/" + month + "/" + day;

			if (!fs::exists(path)) {
				throw "Invalid path.";
				return logs;
			}

			std::vector<std::future<void>> readFutures;

			for (const auto& entry : fs::directory_iterator(path)) {
				readFutures.push_back(std::async(std::launch::async, &LogReader::ReadOp, this, entry.path().string(), &logs));
			}

			for (auto& f : readFutures) {
				f.get();
			}

			return logs;
		}


	private:
		std::string logDirPath;
		std::mutex readMutex;

		// function for async task
		void ReadOp(std::string path, std::vector<std::vector<char>> *logs) {
			std::vector<char> buf;
			Read(path, buf);

			std::lock_guard<std::mutex> lock(readMutex);
			logs->push_back(buf);
		}		
	};
}