#include <iostream>

#include "LogWriter.hpp"
#include "CameraLogSchema_generated.h"

using namespace Modules::NetworkPacket;

namespace example {

	// Write flatbuffers binary to storage

	class CameraLogWriter : public LogWriter<DataBundle> {

	public:

		CameraLogWriter(std::string logDirPath) : LogWriter(logDirPath){}


	private:

		// Inherited via LogWriter
		std::string GetUid(const DataBundle *data){
			return data->data()->size() > 0 ? data->data()->Get(0)->uid()->c_str() : "";
		}

		double GetTimestamp(const DataBundle *data){
			return data->data()->size() > 0 ? data->data()->Get(0)->timestamp() : 0.0;
		}
	};
}