#include <iostream>
#include <vector>
#include <thread>
#include <future>
#include <fmt/core.h>

#include "CameraLogWriter.hpp"
#include "CameraLogParser.hpp"

namespace example {
	
	// Handle incoming flatbuffers data.
	// Read exisiting log files, write incoming logs, print logs.

	class LogManager {

	public:
		LogManager(std::string logDir);

		void WriteToDisk(std::vector<uint8_t> buf, size_t receivedBytes);
		void PrintLogs(const void* buf);
		
		// Read and print existing log file based on client's uid and unix time
		void PrintLogsOf(std::string uid, double utc);
		void PrintLogsOf(std::string uid, std::string year, std::string mon, std::string day);

		void LogManager::WriteReadableLogsToFile(const void* buf);

	private:
		
		std::string logDir;
		std::string lastLogs;

		std::vector<std::future<void>> futures;

		void WriteJob(std::vector<uint8_t>& buf, size_t sz);
		std::string GetReadableLog(CameraLogParser *clp);
	};
}