#include <iostream>
#include <fstream>
#include <filesystem>
#include <iterator>
#include <sstream>
#include <vector>

#include <flatbuffers/flatbuffers.h>

namespace fs = std::filesystem;

namespace example {
	template <typename T> class LogWriter {
	
	public:
		LogWriter(std::string pathToLogDir) {
			pathToBaseDir = pathToLogDir;
		}

		std::string WriteToFile(const T *data, size_t &size) {
			std::string uid = GetUid(data);

			if (uid == "") {
				throw "UID not found.";
				return "";
			}

			// get time
			std::string year = example::TimeUtils::GetYearFromUTC(GetTimestamp(data));
			std::string month = example::TimeUtils::GetMonthFromUTC(GetTimestamp(data));
			std::string date = example::TimeUtils::GetDayFromUTC(GetTimestamp(data));
			std::string time = example::TimeUtils::GetTimeFromUTC(GetTimestamp(data));

			std::string path = pathToBaseDir + "/" + uid + "/" + year + "/"
				+ month + "/" + date;

			fs::create_directories(path);
			std::string filePath = path + "/" + time + ".bytes";

			// write
			Write(filePath, data, size);

			return filePath;
		}

	private:
		std::string pathToBaseDir;
		size_t receivedBytes;

		virtual std::string GetUid(const T *data) = 0;
		virtual double GetTimestamp(const T *data) = 0;
		
		std::size_t GetSize() {
			return this->receivedBytes;
		}

		void Write(std::string filePath, const T *data, size_t &sz) {
			auto buf = flatbuffers::GetBufferStartFromRootPointer(data);
			auto file = std::fstream(filePath, std::ios::out | std::ios::binary);

			file.write((char*)buf, sz);
			file.close();
		}
	};
}