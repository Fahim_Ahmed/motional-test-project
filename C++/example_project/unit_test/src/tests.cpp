#include <gtest/gtest.h>
#include <iostream>
#include <filesystem>
#include <flatbuffers/flatbuffers.h>

#include <PullSocket.hpp>
#include "LogManager.hpp"
#include "LogReader.hpp"
#include <CameraLogSchema_generated.h>
#include "ExternalConfigHandler.hpp"

using namespace example;
using namespace Modules::NetworkPacket;


// Network config tests
struct NetWorkTest : public testing::Test {
    PullSocket *socket;

    void SetUp() {
        std::cout << "socket setup" << std::endl;
        socket = new PullSocket(30000);
    }

    void TearDown() {
        std::cout << "socket delete" << std::endl;
        delete socket;
    }
};

TEST_F(NetWorkTest, InvalidAddress) {
    int rv = socket->ListenForConnections("tcp://127.0.0:1234");
    ASSERT_NE(rv, 0);
}

TEST_F(NetWorkTest, ValidAddress) {
    int rv = socket->ListenForConnections("tcp://127.0.0.1:6120");
    ASSERT_EQ(rv, 0);
}



// Write and read flatbuffers object
struct LogManagerTest : public testing::Test {
    example::CameraLogWriter* writer;
    example::LogReader* reader;

    void SetUp() {
        writer = new example::CameraLogWriter("LOGS");
        reader = new LogReader("LOGS");
    }

    void TearDown() {
        delete writer;
        delete reader;
    }
};

TEST_F(LogManagerTest, WriteFlatBufferToDisk) {
    flatbuffers::FlatBufferBuilder fbb(1024);

    std::vector<flatbuffers::Offset<Info>> bundle;

    int8_t actions[] = { 0, 1 };
    auto actionVector = fbb.CreateVector(actions, 2);

    auto info = CreateInfo(fbb, fbb.CreateString("TEST_123"), 1602560275, &Vec3(1, 2, 3), &Vec4(5, 6, 9, 7), actionVector);
    bundle.push_back(info);

    auto dataVectorOffset = fbb.CreateVector(bundle);

    DataBundleBuilder dbb(fbb);
    dbb.add_data(dataVectorOffset);
    auto db = dbb.Finish();

    FinishDataBundleBuffer(fbb, db);
    auto buf = GetDataBundle(fbb.GetBufferPointer());
    size_t size = fbb.GetSize();

    auto path = writer->WriteToFile(buf, size);
    std::cout << path << std::endl;

    ASSERT_TRUE(std::filesystem::exists(path));
};

TEST_F(LogManagerTest, ReadFlatBufferToDisk) {
    std::vector<char> buffer;
    reader->Read("LOGS/TEST_123/1970/Jan/19/20-09-20.275.bytes", buffer);

    ASSERT_NE(buffer.size(), 0);

    auto db = GetDataBundle(&buffer[0]);
    ASSERT_STREQ(db->data()->Get(0)->uid()->c_str(), "TEST_123");
}


int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}