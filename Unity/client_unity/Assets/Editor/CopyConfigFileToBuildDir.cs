﻿using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;

/// <summary>
/// This will copy our config.ini file from Assets directory to executable directory.
/// </summary>

public class CopyConfigFileToBuildDir : IPostprocessBuildWithReport {
    int IOrderedCallback.callbackOrder => CallbackOrder();

    public void OnPostprocessBuild(BuildReport report) {
        UnityEngine.Debug.Log(report.summary.outputPath);

        FileUtil.CopyFileOrDirectory("./Assets/config.ini", System.IO.Path.GetDirectoryName(report.summary.outputPath) + "\\config.ini");
    }

    int CallbackOrder() {
        return 0;
    }

    
}