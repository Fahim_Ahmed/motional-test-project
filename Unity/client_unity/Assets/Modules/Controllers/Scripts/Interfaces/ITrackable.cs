﻿using UnityEngine;

namespace Modules.Controllers {

    /// <summary>
    /// A interface to make a gameobject trackable.
    /// </summary>


    public interface ITrackable {
        TrackableInfo TrackableInfo { set; get; }
    }

    public struct TrackableInfo {
        public enum ACTION_TYPE {
            ZOOM, PAN, ROTATE
        }

        public double timestamp { get; set; }

        public Vector3 position { get; set; }

        public Quaternion rotation { get; set; }
        
        public string UID { get; set; }

        // User's actions
        public bool isPanning { get; set; }
        public bool isRotating { get; set; }
        public bool isZooming { get; set; }

        public TrackableInfo(string uid) {
            UID = uid;
            timestamp = 0;
            position = Vector3.zero;
            rotation = Quaternion.identity;
            isPanning = false;
            isZooming = false;
            isRotating = false;
        }

        public void SetActionTaken(bool isPanning, bool isZooming, bool isRotating) {
            this.isPanning = isPanning;
            this.isZooming = isZooming;
            this.isRotating = isRotating;
        }
    }
}