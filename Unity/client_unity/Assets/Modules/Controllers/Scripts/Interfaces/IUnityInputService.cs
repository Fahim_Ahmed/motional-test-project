﻿using UnityEngine;

namespace Modules.Controllers {

    /// <summary>
    /// Wrapper for Unity's static input class so we can 
    /// unit tests the user inputs properly.
    /// </summary>
    

    public interface IUnityInputService {
        float GetDeltaTime();
        float GetAxis(string axisName);

        bool GetKey(KeyCode key);

        bool GetMouseButton(int key);
        bool GetMouseButtonDown(int key);
        bool GetMouseButtonUp(int key);

        Vector2 GetMouseScrollDelta();
    }

    public class UnityInputService : IUnityInputService {

        public float GetAxis(string axisName) {
            return Input.GetAxis(axisName);
        }

        public float GetDeltaTime() {
            return Time.deltaTime;
        }

        public bool GetKey(KeyCode key) {
            return Input.GetKey(key);
        }

        public bool GetMouseButton(int key) {
            return Input.GetMouseButton(key);
        }

        public bool GetMouseButtonDown(int key) {
            return Input.GetMouseButtonDown(key);
        }

        public bool GetMouseButtonUp(int key) {
            return Input.GetMouseButtonUp(key);
        }

        public Vector2 GetMouseScrollDelta() {
            return Input.mouseScrollDelta;
        }
    }
}