﻿using UnityEngine;
using TMPro;


namespace Modules.Controllers {

    /// <summary>
    /// A simple component to handle 
    /// connection error text visibility.
    /// </summary>

    public class UIHandler : MonoBehaviour {

        [SerializeField] TMP_Text errText;

        Publisher.DataPublisher dataPublisher;

        void Awake() {
            dataPublisher = Publisher.DataPublisher.Instance;
            dataPublisher.onDataSendError += DataPublisher_onDataSendError;
            dataPublisher.onDataSendSuccess += DataPublisher_onDataSendSuccess;
        }

        private void DataPublisher_onDataSendSuccess() {
            if(errText.gameObject.activeSelf)
                errText.gameObject.SetActive(false);
        }

        private void DataPublisher_onDataSendError(System.Exception ex) {
            if (!errText.gameObject.activeSelf)
                errText.gameObject.SetActive(true);
        }
    }
}