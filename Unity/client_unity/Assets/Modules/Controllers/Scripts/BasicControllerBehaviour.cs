﻿using UnityEngine;

namespace Modules.Controllers {

    public class BasicControllerBehaviour : MonoBehaviour, ITrackable {

        /// <summary>
        /// A first person controller class. Attach it to any 
        /// gameObject to control it with mouse/keyboard.
        /// Notes on control: 
        /// 1. Use mouse to look around / orientation control.
        /// 2. WASD on keyboard to move around - pan left, right and move forward, backward.
        /// 3. EQ on keyboard to pan/move up and down.
        /// 4. Use mouse wheel to zoom in and out ( move forward or backward ).
        /// </summary>

        #region Public variables

        [Header("Control settings")]
        [Tooltip("Target's move speed.")]
        public float moveSpeed = 4f;

        [Tooltip("Mouse look sensitivity.")]
        public float mouseLookSensitivity = 4f;

        public float mouseZoomSpeed = 40f;

        [Header("Smoothings")]
        [Tooltip("Target movement smoothing.")]
        public float moveSmoothing = 10f;

        [Tooltip("Mouse look smoothing.")]
        public float mouseLookSmoothing = 20f;

        [Header("Extras")]
        public bool invertYAxis = false;

        [Tooltip("Press and hold right mouse button for look.")]
        public bool useRightClickToLook = false;

        //[Tooltip("For debugging purpose. Force use of fixed UID.")]
        //public string uid = "GO_1234";

        // User input provider
        public IUnityInputService inputService { get; set; }

        // ITrackable interface properties
        public TrackableInfo TrackableInfo { 
            get {
                trackableInfo.position = transform.position;
                trackableInfo.rotation = transform.rotation;
                return trackableInfo; 
            } 
            set { trackableInfo = value; } }

        #endregion


        #region private variables

        // Target's rotations
        float yaw;      // Y rotation
        float pitch;    // X rotation
        float roll;     // Z rotation

        // value for speed boost on SHIFT press
        float moveSpeedBoost;

        // helper variables for smoothing movements.
        Vector3 lastMoveAmount;
        Quaternion lastRotation;
        TrackableInfo trackableInfo;

        #endregion


        #region Mono methods

        void Awake() {
            inputService = new UnityInputService();
            trackableInfo = new TrackableInfo(Helper.Util.UIDGenerator());
            //trackableInfo = new TrackableInfo(uid);
        }

        void OnEnable() {
            FindObjectOfType<Data.Config.ConfigLoader>()?.SetControllerConfigs(this);

            SetFromTargetTransform();
            InitExtras();
        }

        void Update() {
            ResetActionStatus();

            // Rotate view. 
            HandleLookRotation();

            // Move target. Panning, move forward, and move backward.
            HandleMovements();

            // Cursor visibility on mouse right button press.
            CursorVisibilityHandle(inputService.GetMouseButtonDown(1), inputService.GetMouseButtonUp(1));

            // Update trackable timestamp
            trackableInfo.timestamp = System.DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

        }

        #endregion


        #region private methods

        void HandleLookRotation() {
            Vector2 mouseMovementVector = new Vector2(
                            inputService.GetAxis("Mouse X"),
                            inputService.GetAxis("Mouse Y"));

            if (useRightClickToLook) {

                // Rotate target only when mouse right button is down.
                if (inputService.GetMouseButton(1)) {
                    RotateTarget(mouseMovementVector, inputService.GetDeltaTime());
                }
            }
            else {
                RotateTarget(mouseMovementVector, inputService.GetDeltaTime());
            }
        }

        void HandleMovements() {
            Vector3 movementDirection = GetInputDirectionFromKeyboard();
            bool shiftButtonState = inputService.GetKey(KeyCode.LeftShift);

            // Add zoom in / out input
            float scrollValue = inputService.GetMouseScrollDelta().y * mouseZoomSpeed;
            if (scrollValue != 0) {
                movementDirection.z = scrollValue;
                trackableInfo.isZooming = true;
            }

            MoveTarget(movementDirection, inputService.GetDeltaTime(), shiftButtonState);
        }

        void RotateTarget(Vector2 mouseMovementVector, float deltaTime) {
            mouseMovementVector.y *= (invertYAxis ? 1 : -1);

            Vector2 mouseMoveAmount = mouseMovementVector * mouseLookSensitivity;
            yaw += mouseMoveAmount.y;
            pitch += mouseMoveAmount.x;

            // smoothed rotation value
            Quaternion nextRotation = Quaternion.Slerp(lastRotation, Quaternion.Euler(yaw, pitch, roll), deltaTime *mouseLookSmoothing);

            transform.rotation = nextRotation;

            trackableInfo.isRotating = lastRotation != transform.rotation;

            lastRotation = nextRotation;
        }

        void MoveTarget(Vector3 moveDirection, float deltaTime, bool shiftButtonState) {
            Vector3 moveAmount = moveDirection * deltaTime * moveSpeed;

            // Speed up on SHIFT key held
            if (shiftButtonState) moveAmount *= moveSpeedBoost;

            // smoothed move value
            moveAmount = Vector3.Lerp(lastMoveAmount, moveAmount, deltaTime * moveSmoothing);
            transform.Translate(moveAmount);

            lastMoveAmount = moveAmount;
        }

        /// <summary>
        /// set initial values from current transform
        /// </summary>
        void SetFromTargetTransform() {
            yaw = transform.rotation.x;
            pitch = transform.rotation.y;
            roll = transform.rotation.z;
        }

        void InitExtras() {
            lastMoveAmount = Vector3.zero;
            lastRotation = Quaternion.identity;

            moveSpeedBoost = moveSpeed * 1.5f;

            if (!useRightClickToLook) HideCursor();
        }

        void ResetActionStatus() {
            trackableInfo.SetActionTaken(false, false, false);
        }

        /// <summary>
        /// Hide and lock the cursor on right mouse button down and show again on up.
        /// </summary>
        public void CursorVisibilityHandle(bool mouseRightButtonDownState, bool mouseRightButtonUpState) {
            if (useRightClickToLook) {

                if (mouseRightButtonDownState) {
                    HideCursor();
                }

                if (mouseRightButtonUpState) {
                    ShowCursor();
                }
            }
        }

        void HideCursor() {
            Cursor.lockState = CursorLockMode.Locked;
        }

        void ShowCursor() {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        Vector3 GetInputDirectionFromKeyboard() {
            Vector3 direction = new Vector3();

            if (inputService.GetKey(KeyCode.W)) {
                direction += Vector3.forward;
            }

            if (inputService.GetKey(KeyCode.S)) {
                direction += Vector3.back;
            }

            if (inputService.GetKey(KeyCode.A)) {
                direction += Vector3.left;
                trackableInfo.isPanning = true;
            }

            if (inputService.GetKey(KeyCode.D)) {
                direction += Vector3.right;
                trackableInfo.isPanning = true;
            }

            if (inputService.GetKey(KeyCode.Q)) {
                direction += Vector3.down;
                trackableInfo.isPanning = true;
            }

            if (inputService.GetKey(KeyCode.E)) {
                direction += Vector3.up;
                trackableInfo.isPanning = true;
            }

            return direction;
        }

        #endregion

    }
}

