﻿using UnityEngine;

namespace Modules.Controllers {

    public class CameraControllerBehaviour : BasicControllerBehaviour {

        /// <summary>
        /// First person camera controller class. Similar to what we use in Unity editor.
        /// Notes on control: 
        /// 1. Use mouse to look around.
        /// 2. WASD on keyboard to move around - pan left, right and move forward, backward.
        /// 3. EQ on keyboard to pan or move up and down.
        /// 4. Use mouse wheel to zoom in and out ( move forward or backward ).
        /// </summary>

    }
}