﻿using UnityEngine;

namespace Modules.Controllers.Helper {

    public class Quit : MonoBehaviour {
        void FixedUpdate() {
            if (Input.GetKeyUp(KeyCode.Escape)) Application.Quit();
        }
    }
}