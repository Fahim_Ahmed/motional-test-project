﻿using System.Linq;

namespace Modules.Controllers.Helper {

    public class Util {
        public static string UIDGenerator(string prefix = "GO_", int len = 5) {
            System.Text.StringBuilder builder = new System.Text.StringBuilder();
            Enumerable
               .Range(65, 26)
                .Select(e => ((char) e).ToString())
                .Concat(System.Linq.Enumerable.Range(97, 26).Select(e => ((char) e).ToString()))
                .Concat(System.Linq.Enumerable.Range(0, 10).Select(e => e.ToString()))
                .OrderBy(e => System.Guid.NewGuid())
                .Take(len)
                .ToList().ForEach(e => builder.Append(e));
            return prefix + builder.ToString();
        }

        public static System.DateTimeOffset UnixTimeStampToDateTime(double unixTimeStamp) {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new System.DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }
}