﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace Modules.Controllers {

    /// <summary>
    /// Tracking manager is responsible for collecting all the ITrackable 
    /// objects in the scene. For now, I'm implementing this 
    /// for only one object, which is our camera object.
    /// Also responsible for preparing info for publishing.
    /// </summary>

    public delegate void OnDataReady(byte[] data);

    public class TrackingManager : MonoBehaviour {

        public enum TrackingMode {
            BY_NUMBER,
            BY_TIME
        }

        #region Public variables

        public static TrackingManager Instance { get; private set; }

        [Tooltip("Collect data by a fixed number or by a fixed time before sending it to the publisher.")]
        public TrackingMode trackingMode;

        [Tooltip("Send to publisher when it'll collect x number of trackable info.")]
        public int maxNumberOfInfo = 60;

        [Tooltip("Send to publisher after collecting trackable info of x amount of seconds.")]
        public float maxInfoCollectingTime = 1f;


        // Trigger when 1 chunk of data is ready to publish
        public event OnDataReady onDataReady;

        #endregion


        #region Private variables

        List<TrackableInfo> trackableInfos;
        ITrackable currentTrackable;
        Data.TrackablePacketBuilder packetBuilder;
        
        // helpers
        int currentCollectedInfoNum;
        float elapsedTime;

        float currentAccumulatedDeltaTime = 0;

        #endregion


        #region Mono methods

        void Awake() {
            Instance = this;

            FindObjectOfType<Data.Config.ConfigLoader>()?.SetTrackingManagerConfigs(this);

            trackableInfos = new List<TrackableInfo>();

            currentCollectedInfoNum = 0;
            elapsedTime = 0;

            currentTrackable = FindObjectOfType<BasicControllerBehaviour>().GetComponent<ITrackable>();

            packetBuilder = new Data.TrackablePacketBuilder();
         }

        void LateUpdate() {

            // LateUpdate uses Update's time steps. I want to use FixedUpdate's time step.
            currentAccumulatedDeltaTime += Time.deltaTime;
            if (currentAccumulatedDeltaTime <= Time.fixedDeltaTime) return;

            UpdateTrackableInfos();

            switch (trackingMode) {

                case TrackingMode.BY_NUMBER:
                    HandleInfoByNumber();
                    break;

                case TrackingMode.BY_TIME:
                    HandleInfoByTime();
                    break;
            }

            // Reset delta time counter
            currentAccumulatedDeltaTime = 0;
        }

        #endregion


        #region Private methods

        void HandleInfoByNumber() {
            currentCollectedInfoNum += 1;

            if (currentCollectedInfoNum >= maxNumberOfInfo) {
                PrepareAndNotifyToPublisher(trackableInfos);

                currentCollectedInfoNum = 0;
            }
        }

        void HandleInfoByTime() {
            elapsedTime += currentAccumulatedDeltaTime;

            if (elapsedTime >= maxInfoCollectingTime) {
                PrepareAndNotifyToPublisher(trackableInfos);
                elapsedTime = 0;
            }
        }

        void UpdateTrackableInfos() {
            trackableInfos.Add(currentTrackable.TrackableInfo);
        }

        void PrepareAndNotifyToPublisher(List<TrackableInfo> infos) {
            // convert to flatbuffer object
            byte[] data = packetBuilder.CreateFlatbufferDataFrom(infos);
            NotifyDataIsReady(data);

            // flush data / prepare for next batch
            infos.Clear();
        }

        void NotifyDataIsReady(byte[] fbData) {
            onDataReady?.Invoke(fbData);
        }

        #endregion
    }
}