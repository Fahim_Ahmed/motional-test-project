﻿using UnityEngine;
using Modules.SocketWrapper;

namespace Modules.Publisher {

    /// <summary>
    /// This component is responsible for sending data to 
    /// server.
    /// </summary>

    public delegate void OnDataSendError(System.Exception ex);
    public delegate void OnDataSendSuccess();

    public class DataPublisher : MonoBehaviour {

        public event OnDataSendSuccess onDataSendSuccess;
        public event OnDataSendError onDataSendError;
        public static DataPublisher Instance { get; private set; }


        [Tooltip("Data center URL.")]
        public string serverUrl = "tcp://127.0.0.1";
        
        [Tooltip("Data center URL.")]
        public string port = "6120";

        public bool isConnected { get; set; }

        PushSocket publisherSocket;
        string url;

        void Awake() {
            Instance = this;

            FindObjectOfType<Data.Config.ConfigLoader>()?.SetNetworkConfigs(this);

            url = serverUrl + ":" + port;

            publisherSocket = new PushSocket();
            isConnected = TryConnect(url);
        }

        void Start() {
            if(Controllers.TrackingManager.Instance)
                Controllers.TrackingManager.Instance.onDataReady += Instance_onDataReady;
        }

        void OnDisable() {
            if (Controllers.TrackingManager.Instance) 
                Controllers.TrackingManager.Instance.onDataReady -= Instance_onDataReady;
        }

        void Instance_onDataReady(byte[] data) {
            Send(data);
        }

        public bool TryConnect(string url) {
            try {
                publisherSocket.Dial(url);
            }
            catch (System.Exception ex) {
                onDataSendError?.Invoke(ex);
                return false;
            }

            return true;
        }

        public void Send(byte[] data) {
            try {
                publisherSocket.Send(data);
            }
            catch (System.Exception ex) {
                onDataSendError?.Invoke(ex);
                Debug.Log(ex.Message);
                return;
            }

            onDataSendSuccess?.Invoke();
        }
    }
}