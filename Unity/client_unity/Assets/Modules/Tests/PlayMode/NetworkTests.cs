﻿using NUnit.Framework;
using Modules.SocketWrapper;

namespace Tests {

    public class NetworkTests {

        //[Test]
        public void Test_Network_Connection() {
            // Assuming server is up and running.

            var socket = new PushSocket();
            socket.Dial("tcp://127.0.0.1:6120");
            socket.Send(new byte[1]);

            Assert.IsTrue(socket.isConnected);
        }

        [Test]
        public void Test_Network_Connection_Fail() {
            // Assuming server is not running.

            var socket = new PushSocket();
            socket.Dial("tcp://127.0.0.1:6120");
            

            Assert.That(() => socket.Send(new byte[1]), Throws.TypeOf<System.Exception>());
        }
    }
}