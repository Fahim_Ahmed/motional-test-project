﻿using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;
using NSubstitute;
using NUnit.Framework;

namespace Tests {
    public class CameraControllerTests {

        /// <summary>
        /// Testcases for 
        /// Camera left, right, up, down
        /// Look left, right
        /// Zoom in and out
        /// </summary>
        

        [UnityTest]
        public IEnumerator Mouse_Look_Right_Test() {
            var cameraController = new GameObject().AddComponent<Modules.Controllers.CameraControllerBehaviour>();

            var inputService = Substitute.For<Modules.Controllers.IUnityInputService>();
            inputService.GetAxis("Mouse X").Returns(1f);
            inputService.GetAxis("Mouse Y").Returns(0);
            inputService.GetDeltaTime().Returns(1);

            cameraController.inputService = inputService;

            Vector3 pastForward = cameraController.transform.forward;

            yield return null;

            float dir = Vector3.Cross(cameraController.transform.forward, pastForward).y;
            Assert.Less(dir, 0f);
        }

        [UnityTest]
        public IEnumerator Mouse_Look_left_Test() {
            var cameraController = new GameObject().AddComponent<Modules.Controllers.CameraControllerBehaviour>();

            var inputService = Substitute.For<Modules.Controllers.IUnityInputService>();
            inputService.GetAxis("Mouse X").Returns(-1f);
            inputService.GetAxis("Mouse Y").Returns(0);
            inputService.GetDeltaTime().Returns(1);

            cameraController.inputService = inputService;

            Vector3 pastForward = cameraController.transform.forward;

            yield return null;

            float dir = Vector3.Cross(cameraController.transform.forward, pastForward).y;
            Assert.Greater(dir, 0f);
        }

        [UnityTest]
        public IEnumerator Mouse_Look_Up_Test() {
            var cameraController = new GameObject().AddComponent<Modules.Controllers.CameraControllerBehaviour>();

            var inputService = Substitute.For<Modules.Controllers.IUnityInputService>();
            inputService.GetAxis("Mouse X").Returns(0);
            inputService.GetAxis("Mouse Y").Returns(1f);
            inputService.GetDeltaTime().Returns(1);

            cameraController.inputService = inputService;

            Vector3 pastForward = cameraController.transform.forward;

            yield return null;

            float dir = Vector3.Cross(cameraController.transform.forward, pastForward).x;
            Assert.Greater(dir, 0);
        }

        [UnityTest]
        public IEnumerator Mouse_Look_Down_Test() {
            var cameraController = new GameObject().AddComponent<Modules.Controllers.CameraControllerBehaviour>();

            var inputService = Substitute.For<Modules.Controllers.IUnityInputService>();
            inputService.GetAxis("Mouse X").Returns(0);
            inputService.GetAxis("Mouse Y").Returns(-1f);
            inputService.GetDeltaTime().Returns(1);

            cameraController.inputService = inputService;

            Vector3 pastForward = cameraController.transform.forward;

            yield return null;

            float dir = Vector3.Cross(cameraController.transform.forward, pastForward).x;
            Assert.Less(dir, 0);
        }

        [UnityTest]
        public IEnumerator Move_Left_Test() {
            var cameraController = new GameObject().AddComponent<Modules.Controllers.CameraControllerBehaviour>();

            var inputService = Substitute.For<Modules.Controllers.IUnityInputService>();
            inputService.GetKey(KeyCode.A).Returns(true);
            inputService.GetDeltaTime().Returns(1);

            cameraController.inputService = inputService;

            Vector3 pastPos = cameraController.transform.position;

            yield return null;

            Assert.Less(cameraController.transform.position.x, pastPos.x);
        }

        [UnityTest]
        public IEnumerator Move_Right_Test() {
            var cameraController = new GameObject().AddComponent<Modules.Controllers.CameraControllerBehaviour>();

            var inputService = Substitute.For<Modules.Controllers.IUnityInputService>();
            inputService.GetKey(KeyCode.D).Returns(true);
            inputService.GetDeltaTime().Returns(1);

            cameraController.inputService = inputService;

            Vector3 pastPos = cameraController.transform.position;

            yield return null;

            Assert.Greater(cameraController.transform.position.x, pastPos.x);
        }

        [UnityTest]
        public IEnumerator Move_Forward_Test() {
            var cameraController = new GameObject().AddComponent<Modules.Controllers.CameraControllerBehaviour>();

            var inputService = Substitute.For<Modules.Controllers.IUnityInputService>();
            inputService.GetKey(KeyCode.W).Returns(true);
            inputService.GetDeltaTime().Returns(1);

            cameraController.inputService = inputService;

            Vector3 pastPos = cameraController.transform.position;

            yield return null;

            Assert.Greater(cameraController.transform.position.z, pastPos.z);
        }

        [UnityTest]
        public IEnumerator Move_Backward_Test() {
            var cameraController = new GameObject().AddComponent<Modules.Controllers.CameraControllerBehaviour>();

            var inputService = Substitute.For<Modules.Controllers.IUnityInputService>();
            inputService.GetKey(KeyCode.S).Returns(true);
            inputService.GetDeltaTime().Returns(1);

            cameraController.inputService = inputService;

            Vector3 pastPos = cameraController.transform.position;

            yield return null;

            Assert.Less(cameraController.transform.position.z, pastPos.z);
        }

        [UnityTest]
        public IEnumerator Zoom_In_Test() {
            var cameraController = new GameObject().AddComponent<Modules.Controllers.CameraControllerBehaviour>();

            var inputService = Substitute.For<Modules.Controllers.IUnityInputService>();
            inputService.GetMouseScrollDelta().Returns(new Vector2(0, 1f));
            inputService.GetDeltaTime().Returns(1);

            cameraController.inputService = inputService;

            Vector3 pastPos = cameraController.transform.position;

            yield return null;

            Assert.Greater(cameraController.transform.position.z, pastPos.z);
        }

        [UnityTest]
        public IEnumerator Zoom_Out_Test() {
            var cameraController = new GameObject().AddComponent<Modules.Controllers.CameraControllerBehaviour>();

            var inputService = Substitute.For<Modules.Controllers.IUnityInputService>();
            inputService.GetMouseScrollDelta().Returns(new Vector2(0, -1f));
            inputService.GetDeltaTime().Returns(1);

            cameraController.inputService = inputService;

            Vector3 pastPos = cameraController.transform.position;

            yield return null;

            Assert.Less(cameraController.transform.position.z, pastPos.z);
        }
    }
}