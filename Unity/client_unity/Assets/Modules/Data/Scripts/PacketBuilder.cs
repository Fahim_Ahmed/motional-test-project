﻿using System.Collections.Generic;

namespace Modules.Data {

    public abstract class PacketBuilder<T> {
        public abstract byte[] CreateFlatbufferDataFrom(List<T> infos);
    }
}