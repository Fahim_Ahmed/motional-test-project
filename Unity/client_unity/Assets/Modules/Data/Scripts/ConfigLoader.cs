﻿using UnityEngine;
using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Modules.Data.Config {

    public class ConfigLoader : MonoBehaviour {


#if UNITY_EDITOR
        string configFilePath = "./Assets/config.ini";
#else
        string configFilePath = "./config.ini";
#endif

        public void SetControllerConfigs(Controllers.BasicControllerBehaviour basicController) {
            StringBuilder val = new StringBuilder();

            ExternalConfigHandler.GetPrivateProfileString("camera controller configs", "move_speed", "4.0", val, 10, configFilePath);
            if(Marshal.GetLastWin32Error() != 0) Debug.LogWarning("Config file error. Error code: " + Marshal.GetLastWin32Error());

            basicController.moveSpeed = float.Parse(val.ToString());

            ExternalConfigHandler.GetPrivateProfileString("camera controller configs", "mouse_look_sensitivity", "4.0", val, 10, configFilePath);
            basicController.mouseLookSensitivity = float.Parse(val.ToString());

            ExternalConfigHandler.GetPrivateProfileString("camera controller configs", "zoom_speed", "40.0", val, 10, configFilePath);
            basicController.mouseZoomSpeed = float.Parse(val.ToString());

            ExternalConfigHandler.GetPrivateProfileString("camera controller configs", "move_smoothness", "10.0", val, 10, configFilePath);
            basicController.moveSmoothing = float.Parse(val.ToString());

            ExternalConfigHandler.GetPrivateProfileString("camera controller configs", "mouse_look_smoothness", "30.0", val, 10, configFilePath);
            basicController.mouseLookSmoothing = float.Parse(val.ToString());

            ExternalConfigHandler.GetPrivateProfileString("camera controller configs", "invertY", "false", val, 10, configFilePath);
            basicController.invertYAxis = bool.Parse(val.ToString());

            ExternalConfigHandler.GetPrivateProfileString("camera controller configs", "use_right_click_look", "false", val, 10, configFilePath);
            basicController.useRightClickToLook = bool.Parse(val.ToString());
        }

        public void SetNetworkConfigs(Publisher.DataPublisher dataPublisher) {
            StringBuilder val = new StringBuilder();

            ExternalConfigHandler.GetPrivateProfileString("network configs", "server_url", "tcp://127.0.0.1", val, 20, configFilePath);
            dataPublisher.serverUrl = val.ToString();

            ExternalConfigHandler.GetPrivateProfileString("network configs", "server_port", "6120", val, 10, configFilePath);
            dataPublisher.port = val.ToString();
        }

        public void SetTrackingManagerConfigs(Controllers.TrackingManager trackingManager) {
            StringBuilder val = new StringBuilder();

            ExternalConfigHandler.GetPrivateProfileString("network configs", "tracking_mode", "BY_TIME", val, 20, configFilePath);
            Enum.TryParse<Controllers.TrackingManager.TrackingMode>(val.ToString(), out trackingManager.trackingMode);

            ExternalConfigHandler.GetPrivateProfileString("network configs", "data_collection_time", "1", val, 10, configFilePath);
            trackingManager.maxInfoCollectingTime = float.Parse(val.ToString());
            
            ExternalConfigHandler.GetPrivateProfileString("network configs", "data_collection_number", "100", val, 10, configFilePath);
            trackingManager.maxNumberOfInfo = int.Parse(val.ToString());
        }
    }
}