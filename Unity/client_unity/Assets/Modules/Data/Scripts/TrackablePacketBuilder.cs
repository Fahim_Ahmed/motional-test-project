﻿using UnityEngine;
using FlatBuffers;
using Modules.NetworkPacket;
using Modules.Controllers;
using System.Collections.Generic;

namespace Modules.Data {

    /// <summary>
    /// Subclass of PacketBuilder. Receives TrackableInfo objects, and 
    /// pack as flatbuffers object, and returns that to caller.
    /// </summary>

    public class TrackablePacketBuilder : PacketBuilder<TrackableInfo> {
        public override byte[] CreateFlatbufferDataFrom(List<TrackableInfo> infos) {
            FlatBufferBuilder fbb = new FlatBufferBuilder(64);

            Offset<Info>[] dataBundle = new Offset<Info>[infos.Count];

            for (int i = 0; i < infos.Count; i++) {
                dataBundle[i] = CreateFlatbufferDataFrom(infos[i], ref fbb);
            }

            var dataVectorOffset = Info.CreateSortedVectorOfInfo(fbb, dataBundle);

            DataBundle.StartDataBundle(fbb);
            DataBundle.AddData(fbb, dataVectorOffset);
            var root = DataBundle.EndDataBundle(fbb);

            fbb.Finish(root.Value);

            return fbb.SizedByteArray();
        }

        Offset<Info> CreateFlatbufferDataFrom(TrackableInfo info, ref FlatBufferBuilder fbb) {
            Vector3 p = info.position;
            Quaternion r = info.rotation;

            var actionTaken = GetTakenActionsFrom(info);

            Info.StartActionTakenVector(fbb, actionTaken.Length);
            for (int j = 0; j < actionTaken.Length; j++) {
                fbb.AddSbyte((sbyte) actionTaken[j]);
            }
            var actions = fbb.EndVector();

            var uid = fbb.CreateString(info.UID);

            Info.StartInfo(fbb);
            Info.AddUid(fbb, uid);
            Info.AddPosition(fbb, Vec3.CreateVec3(fbb, p.x, p.y, p.z));
            Info.AddRotation(fbb, Vec4.CreateVec4(fbb, r.x, r.y, r.z, r.w));
            Info.AddTimestamp(fbb, info.timestamp);
            Info.AddActionTaken(fbb, actions);

            return Info.EndInfo(fbb);
        }

        TrackableInfo.ACTION_TYPE[] GetTakenActionsFrom(TrackableInfo trackable) {
            List<TrackableInfo.ACTION_TYPE> actionTaken = new List<TrackableInfo.ACTION_TYPE>();
            if (trackable.isZooming) actionTaken.Add(TrackableInfo.ACTION_TYPE.ZOOM);
            if (trackable.isPanning) actionTaken.Add(TrackableInfo.ACTION_TYPE.PAN);
            if (trackable.isRotating) actionTaken.Add(TrackableInfo.ACTION_TYPE.ROTATE);

            return actionTaken.ToArray();
        }
    }
}