﻿using System.Runtime.InteropServices;
using System.Text;

namespace Modules.Data.Config {

    /// <summary>
    /// Import windows's dll to read ini file.
    /// Definitely not a cross-platform solution...
    /// </summary>

    public class ExternalConfigHandler {

        [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileString", SetLastError = true)]
        public static extern int GetPrivateProfileString(string SectionName, string KeyName, string Default, StringBuilder Return_StringBuilder_Name, int Size, string FileName);
        
        [DllImport("kernel32.dll", EntryPoint = "GetLastError")]
        public static extern int GetLastError();
    }
}