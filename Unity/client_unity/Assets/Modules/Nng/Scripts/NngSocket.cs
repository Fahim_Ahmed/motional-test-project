﻿/**
 * \copyright
 * Copyright (c) 2019- Aptiv. All rights reserved.
 * NOTICE: Removing this heading is prohibited.
 * All information contained herein is, and remains the property of Aptiv and its suppliers, if any.
 * The intellectual and technical concepts contained herein are proprietary to Aptiv and its suppliers and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Aptiv.
 */

using System;
using System.Text;
using Microsoft.Win32.SafeHandles;
using static Modules.Nng.Native;
//using Example.Modules.Nng.ByteBuffer;

namespace Modules.Nng
{
    /// <summary>
    /// Nng socket handle that adheres to RAII, also includes common functionality for sockets
    /// </summary>
    public abstract class NngSocket : SafeHandleZeroOrMinusOneIsInvalid
    {
        public NngSocket() : base(true)
        {
        }

        protected override bool ReleaseHandle()
        {
            var startTime = DateTime.Now;
            while ((DateTime.Now - startTime) < TimeSpan.FromSeconds(3))
            {
                if((Nng_errno_enum)nng_close((UInt32)handle) != Nng_errno_enum.NNG_EINTR)
                {
                    break;
                }
                System.Threading.Thread.Sleep(20);
            }
            return true;
        }

        public Nng_errno_enum SetOpt(StringBuilder opt, byte[] value)
        {
            using (var pinned = new MemoryPinner(value))
            {
                return (Nng_errno_enum)nng_setopt((UInt32)handle, opt, pinned.ptr, (ulong)value.Length);
            }
        }

        public Nng_errno_enum SetOptMs(StringBuilder opt, Int32 time)
        {
            return (Nng_errno_enum)nng_setopt_ms((UInt32)handle, opt, time);
        }

        public Nng_errno_enum SetOptSize(StringBuilder opt, UInt32 value)
        {
            return (Nng_errno_enum)nng_setopt_size((UInt32)handle, opt, value);
        }
    }

    /// <summary>
    /// Push protocol socket
    /// </summary>
    public class PushProtocolSocket : NngSocket {
        public PushProtocolSocket() {
            UInt32 socket;
            nng_push0_open(out socket);
            handle = (IntPtr) socket;
        }

        public Nng_errno_enum Dial(string url, nng_flag_enum flags = nng_flag_enum.NNG_FLAG_NONBLOCK) {
            return (Nng_errno_enum) nng_dial((UInt32) handle, new StringBuilder(url), IntPtr.Zero, flags);
        }

        public Nng_errno_enum Send(byte[] data) {
            using (var pinned = new MemoryPinner(data)) {
                return (Nng_errno_enum) nng_send((UInt32) handle, pinned.ptr, (ulong) data.Length, nng_flag_enum.NNG_FLAG_NONBLOCK);
            }
        }
    }

    /// <summary>
    /// Publisher socket
    /// </summary>
    public class PubSocket : NngSocket
    {
        public PubSocket()
        {
            UInt32 socket;
            nng_pub0_open(out socket);
            handle = (IntPtr)socket;
        }

        public Nng_errno_enum Listen(string url, nng_flag_enum flags = nng_flag_enum.NNG_FLAG_NONE)
        {
            return (Nng_errno_enum)nng_listen((UInt32)handle, new StringBuilder(url), IntPtr.Zero, flags);
        }

        public Nng_errno_enum Send(byte[] data)
        {
            using (var pinned = new MemoryPinner(data))
            {
                return (Nng_errno_enum)nng_send((UInt32)handle, pinned.ptr, (ulong)data.Length, nng_flag_enum.NNG_FLAG_NONBLOCK);
            }
        }
    }

    /// <summary>
    /// Subscriber socket
    /// </summary>
    public class SubSocket : NngSocket
    {
        public SubSocket()
        {
            UInt32 socket;
            nng_sub0_open(out socket);
            handle = (IntPtr)socket;
        }
        public Nng_errno_enum SubscribeAll()
        {
            return SetOpt(Native.NNG_OPT_SUB_SUBSCRIBE, Encoding.ASCII.GetBytes(""));
        }
        public Nng_errno_enum Subscribe(byte[] prefix)
        {
            return SetOpt(Native.NNG_OPT_SUB_SUBSCRIBE, prefix);
        }
        public Nng_errno_enum Dial(string url, nng_flag_enum flags = nng_flag_enum.NNG_FLAG_NONE)
        {
            return (Nng_errno_enum)nng_dial((UInt32)handle, new StringBuilder(url), IntPtr.Zero, flags);
        }
        public Nng_errno_enum Recv(byte[] data, ref UInt64 size)
        {

            UInt64 maxSize = (UInt64) data.Length;
            size = maxSize;
            using (var pinned = new MemoryPinner(data))
            {
                var err = (Nng_errno_enum)nng_recv((UInt32)handle, pinned.ptr, ref size, nng_flag_enum.NNG_FLAG_NONBLOCK);
                if(size > maxSize)
                {
                    throw new Exception($"Motional.Modules.Nng.SubSocket.Recv message length exceeded. buffer size:{data.Length} incoming msg size:{size}");
                }
                return err;
            }
        }
    }
    
    /// <summary>
    /// Functionality for obtain the error message from error enums
    /// </summary>
    public class ErrorEnumMessage : Attribute
    {
        string message;
        public ErrorEnumMessage(string message)
        {
            this.message = message;
        }
        public static string GetErrorMessage(Nng_errno_enum value)
        {
            var type = value.GetType();
            var memInfo = type.GetMember(value.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(ErrorEnumMessage), false);
            return (attributes.Length > 0) ? (attributes[0] as ErrorEnumMessage).message : "";
        }
    }
    /// <summary>
    /// Error enums and their coresponding error messages from nng
    /// </summary>
    public enum Nng_errno_enum : int
    {
        NNG_OK = 0,
        [ErrorEnumMessage("Interrupted")]
        NNG_EINTR = 1,
        [ErrorEnumMessage("Out of memory")]
        NNG_ENOMEM = 2,
        [ErrorEnumMessage("Invalid argument")]
        NNG_EINVAL = 3,
        [ErrorEnumMessage("Resource busy")]
        NNG_EBUSY = 4,
        [ErrorEnumMessage("Timed out")]
        NNG_ETIMEDOUT = 5,
        [ErrorEnumMessage("Connection refused")]
        NNG_ECONNREFUSED = 6,
        [ErrorEnumMessage("Object closed")]
        NNG_ECLOSED = 7,
        [ErrorEnumMessage("Try again")]
        NNG_EAGAIN = 8,
        [ErrorEnumMessage("Not supported")]
        NNG_ENOTSUP = 9,
        [ErrorEnumMessage("Address in use")]
        NNG_EADDRINUSE = 10,
        [ErrorEnumMessage("Incorrect state")]
        NNG_ESTATE = 11,
        [ErrorEnumMessage("Entry not found")]
        NNG_ENOENT = 12,
        [ErrorEnumMessage("Protocol error")]
        NNG_EPROTO = 13,
        [ErrorEnumMessage("Destination unreachable")]
        NNG_EUNREACHABLE = 14,
        [ErrorEnumMessage("Address invalid")]
        NNG_EADDRINVAL = 15,
        [ErrorEnumMessage("Permission denied")]
        NNG_EPERM = 16,
        [ErrorEnumMessage("Message too large")]
        NNG_EMSGSIZE = 17,
        [ErrorEnumMessage("Connection aborted")]
        NNG_ECONNABORTED = 18,
        [ErrorEnumMessage("Connection reset")]
        NNG_ECONNRESET = 19,
        [ErrorEnumMessage("Operation canceled")]
        NNG_ECANCELED = 20,
        [ErrorEnumMessage("Out of files")]
        NNG_ENOFILES = 21,
        [ErrorEnumMessage("Out of space")]
        NNG_ENOSPC = 22,
        [ErrorEnumMessage("Resource already exists")]
        NNG_EEXIST = 23,
        [ErrorEnumMessage("Read only resource")]
        NNG_EREADONLY = 24,
        [ErrorEnumMessage("Write only resource")]
        NNG_EWRITEONLY = 25,
        [ErrorEnumMessage("Cryptographic error")]
        NNG_ECRYPTO = 26,
        [ErrorEnumMessage("Peer could not be authenticated")]
        NNG_EPEERAUTH = 27,
        [ErrorEnumMessage("Option requires argument")]
        NNG_ENOARG = 28,
        [ErrorEnumMessage("Ambiguous option")]
        NNG_EAMBIGUOUS = 29,
        [ErrorEnumMessage("Incorrect type")]
        NNG_EBADTYPE = 30,
        [ErrorEnumMessage("Internal error detected")]
        NNG_EINTERNAL = 1000,
        [ErrorEnumMessage("System Error")]
        NNG_ESYSERR = 0x10000000,
        [ErrorEnumMessage("Transport Error")]
        NNG_ETRANERR = 0x20000000
    };
}