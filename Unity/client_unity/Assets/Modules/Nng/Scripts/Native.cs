/**
 * \copyright
 * Copyright (c) 2019- Aptiv. All rights reserved.
 * NOTICE: Removing this heading is prohibited.
 * All information contained herein is, and remains the property of Aptiv and its suppliers, if any.
 * The intellectual and technical concepts contained herein are proprietary to Aptiv and its suppliers and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Aptiv.
 */

using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Modules.Nng
{
    /// <summary>
    /// Interface to native nng library, refer to https://nanomsg.github.io/nng/man/tip/ for documentation
    /// </summary>
    public static class Native
    {
        [DllImport("nng")]
        public extern static int nng_pub0_open(out UInt32 s);
        [DllImport("nng")]
        public extern static int nng_sub0_open(out UInt32 s);
        [DllImport("nng")]
        public extern static int nng_close(UInt32 s);
        [DllImport("nng")]
        public extern static int nng_free(IntPtr ptr, UInt64 size);
        [DllImport("nng")]
        public extern static int nng_listen(UInt32 s, StringBuilder url, IntPtr lp, nng_flag_enum flags);
        [DllImport("nng")]
        public extern static int nng_send(UInt32 s, IntPtr data, UInt64 size, nng_flag_enum flags);
        [DllImport("nng")]
        public extern static int nng_recv(UInt32 s, IntPtr data, ref UInt64 sizep, nng_flag_enum flags);
        [DllImport("nng")]
        public extern static int nng_setopt(UInt32 s, StringBuilder opt, IntPtr val, UInt64 valsz);
        [DllImport("nng")]
        public extern static int nng_setopt_size(UInt32 s, StringBuilder opt, UInt64 valsz);
        [DllImport("nng")]
        public extern static int nng_setopt_ms(UInt32 s, StringBuilder opt, Int32 duration);
        [DllImport("nng")]
        public extern static int nng_dial(UInt32 s, StringBuilder url, IntPtr dp, nng_flag_enum flags);
        
        [DllImport("nng")]
        public extern static int nng_push0_open(out UInt32 s);


        public enum nng_flag_enum : int
        {
            NNG_FLAG_NONE = 0,
            NNG_FLAG_ALLOC = 1, // Recv to allocate receive buffer.
            NNG_FLAG_NONBLOCK = 2  // Non-blocking operations.
        };

        public static readonly StringBuilder NNG_OPT_SUB_SUBSCRIBE = new StringBuilder("sub:subscribe");
        public static readonly StringBuilder NNG_OPT_RECVMAXSZ = new StringBuilder("recv-size-max");
        public static readonly StringBuilder NNG_OPT_RECONNMINT = new StringBuilder("reconnect-time-min");
        public static readonly StringBuilder NNG_OPT_RECONNMAXT = new StringBuilder("reconnect-time-max");
    }
}
