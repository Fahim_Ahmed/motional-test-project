/**
 * \copyright
 * Copyright (c) 2019- Aptiv. All rights reserved.
 * NOTICE: Removing this heading is prohibited.
 * All information contained herein is, and remains the property of Aptiv and its suppliers, if any.
 * The intellectual and technical concepts contained herein are proprietary to Aptiv and its suppliers and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Aptiv.
 */

using System;
using System.Runtime.InteropServices;

namespace Modules.Nng
{
    /// <summary>
    /// Functionality to prevent garbage collector from moving a dynamically allocated object,
    /// in order to use the managed object for native functions.
    /// </summary>
    public class MemoryPinner : IDisposable
    {
        GCHandle handle;
        public IntPtr ptr { private set; get; }

        public MemoryPinner(object toPin)
        {
            handle = GCHandle.Alloc(toPin, GCHandleType.Pinned);
            ptr = handle.AddrOfPinnedObject();
        }

        public void Dispose()
        {
            handle.Free();
        }
    }
}