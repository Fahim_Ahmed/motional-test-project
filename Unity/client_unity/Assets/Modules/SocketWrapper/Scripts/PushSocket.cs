﻿using System;
using Modules.Nng;

namespace Modules.SocketWrapper {

    /// <summary>
    /// NNG Push pipeline.
    /// </summary>

    public class PushSocket : IDisposable {

        public bool isConnected { get; set; }
        
        PushProtocolSocket m_Socket;

        public PushSocket() {
            m_Socket = new PushProtocolSocket();
        }

        /// <summary>
        /// Binds to a provided endpoint
        /// </summary>
        public void Dial(string url) {
            var val = m_Socket.Dial(url);
            if (val != Nng_errno_enum.NNG_OK) {
                Error(val);
                return;
            }

            isConnected = true;
        }

        /// <summary>
        /// Take an existing finished byteBuffer and sends it.
        /// </summary>
        public void Send(byte[] byteBuffer) {
            var val = m_Socket.Send(byteBuffer);
            if (val != Nng_errno_enum.NNG_OK) {
                Error(val);
                return;
            }

            isConnected = true;
        }

        public void Dispose() {
            m_Socket.Dispose();
            m_Socket = null;
        }

        void Error(Nng_errno_enum val) {
            isConnected = false;
            throw new Exception(ErrorEnumMessage.GetErrorMessage(val));
        }
    }
}