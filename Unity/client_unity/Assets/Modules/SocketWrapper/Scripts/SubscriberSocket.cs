﻿/**
 * \copyright
 * Copyright (c) 2019- Aptiv. All rights reserved.
 * NOTICE: Removing this heading is prohibited.
 * All information contained herein is, and remains the property of Aptiv and its suppliers, if any.
 * The intellectual and technical concepts contained herein are proprietary to Aptiv and its suppliers and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Aptiv.
 */

using System;
//using System.Text;
using Modules.Nng;
//using System.Collections.Generic;
//using System.Linq;
//using UnityEngine;

namespace Modules.SocketWrapper
{
    /// <summary>
    /// Subscriber socket
    /// </summary>
    public class SubscriberSocket : IDisposable
    {
        public const Int32 ReconnectionMinTimeMs = 10;
        public const Int32 ReconnectionMaxTimeMs = 500;

        SubSocket m_Socket = new SubSocket();
        byte[] m_Buffer;

        public SubscriberSocket(uint maxLengthOfMsg)
        {
            m_Buffer = new byte[maxLengthOfMsg];
            m_Socket.SetOptSize(Native.NNG_OPT_RECVMAXSZ, 0); // unbounded max message size
            m_Socket.SetOptMs(Native.NNG_OPT_RECONNMINT, ReconnectionMinTimeMs);
            m_Socket.SetOptMs(Native.NNG_OPT_RECONNMAXT, ReconnectionMaxTimeMs);
            m_Socket.SubscribeAll();
        }
        public void Dispose()
        {
            m_Socket.Dispose();
            m_Socket = null;
        }

        public void DialToServer(string url)
        {
            var val = m_Socket.Dial(url, Native.nng_flag_enum.NNG_FLAG_NONBLOCK);
            if (val != Nng_errno_enum.NNG_OK)
            {
                throw new Exception(ErrorEnumMessage.GetErrorMessage(val)); // Actual error
            }
        }

        /// <summary>
        /// subscribe to all messages
        /// </summary>
        public void SubscribeAll()
        {
            m_Socket.SubscribeAll();
        }
        /// <summary>
        /// subscribe to a message code
        /// </summary>
        public void SubscribeMessage(byte[] prefix)
        {
            m_Socket.Subscribe(prefix);
        }

        /// <summary>
        /// If a message is avaible, retrives the flatbuffer contents and the type
        /// </summary>
        public bool PollForRecievedData(ref byte[] bb, out uint bufferSize)
        {
            UInt64 receivedBytes = 0;
            if(m_Socket.Recv(m_Buffer, ref receivedBytes) == Nng_errno_enum.NNG_OK)
            {
                bb = new byte[receivedBytes];
                Buffer.BlockCopy(m_Buffer, 0, bb, 0, (int)receivedBytes);
                bufferSize = (uint)receivedBytes;
                return true;
            }

            bufferSize = 0;
            return false;
        }
    }
}
