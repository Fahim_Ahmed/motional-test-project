﻿/**
 * \copyright
 * Copyright (c) 2019- Motional. All rights reserved.
 * NOTICE: Removing this heading is prohibited.
 * All information contained herein is, and remains the property of Motional and its suppliers, if any.
 * The intellectual and technical concepts contained herein are proprietary to Motional and its suppliers and may be
 * covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Motional.
 */

using System;
using Modules.Nng;

namespace Modules.SocketWrapper
{
    /// <summary>
    /// Publisher socket with support for seperate channels for different flatbuffer messages
    /// </summary>
    public class PublishingSocket : IDisposable
    {
        PubSocket m_Socket;

        public PublishingSocket()
        {
            m_Socket = new PubSocket();
        }
        
        public void Dispose()
        {
            m_Socket.Dispose();
            m_Socket = null;
        }
        
        /// <summary>
        /// Binds to a provided endpoint
        /// </summary>
        public void Listen(string url)
        {
            var val = m_Socket.Listen(url);
            if(val != Nng_errno_enum.NNG_OK)
            {
                throw new Exception(ErrorEnumMessage.GetErrorMessage(val));
            }
        }
        
        /// <summary>
        /// Take an existing finished byteBuffer and sends it.
        /// </summary>
        public void Send(byte[] byteBuffer)
        {
            var val = m_Socket.Send(byteBuffer);
            if(val != Nng_errno_enum.NNG_OK)
            {
                throw new Exception(ErrorEnumMessage.GetErrorMessage(val));
            }
        }
    }
}
