
# Test Project

## Overview

**Please download the whole project including executables from:** [HERE](https://bitbucket.org/Fahim_Ahmed/motional-test-project/downloads/MotionalTestProject.zip)

#### Dev environment
- Windows 10 Pro 64bit build 19041.508
- Visual Studio 16 2019
- Unity 2019.4.10f1 LTS
- CMake version used 3.14
 

#### Directory structure
```
├─── Readme.md                   # this file
├─── Basic_architectural_overview    
│ 
├─── example_project          # C++ sources
│    ├─── common              # common files used by other
│    ├─── logging             # logging unit
│    ├─── server              # main server
│    ├─── unit_test           # tests
│    │    ├───conan           # conan config files for Google test frameworks integration
│    └─── third_party
│         ├─── flatbuffers
│         ├─── fmt
│         └─── nng
│
├─── FlatBuffersSchema        # Schema file and generator for flatbuffers
│ 
├─── Unity
│    └─── client_unity        # Unity project
│ 
└─── _Executables             # Builds
     ├─── C++ Server
     ├─── Tests
     └─── Unity Build
```

#### Build commands used for C++ sources

```
cd C++
mkdir build
cd build
cmake .. -G "Visual Studio 16 2019"
cmake --build .
```

## Getting started

### C++ server / Listening server

Server executable location "_Executables\C++ Server\example_server.exe"
Configuration file for server "_Executables\C++ Server\config.ini"

Configuration parameters in config.ini file are mostly self explanatory. Set the parameter and launch the executable.
Notes for the 4 settings under "log configs" section are:

```
- log_dir_path="LOGS"   : Directory at which log files are going to save.
- print_on_console=1    : Print incoming client's data on console.
- write_to_disk=1       : Write incoming flatbuffers object of each client to storage. Basically they will be written on "LOGS" directory. 
- write_logs_to_disk=1  : Write human readable logs of each client in log files. 
```

Example structure of LOGS directory:
```
│─── GO_udKF4.log
│─── GO_UX1eL.log
│─── GO_ISJeL.log
│
├─── GO_udKF4
│    └─── 2020
│         └─── Oct
│              └─── 12
│                       21-44-31.457.bytes
│                       21-44-32.143.bytes
│                       21-44-32.645.bytes
│   
├─── GO_UX1eL  
│    └─── 2020  
│         └─── Oct 
│              └─── 12
│                     21-23-11.127.bytes
│                     21-23-11.617.bytes
│ 
└─── GO_ISJeL
     └─── 2020 
          └─── Oct
               └─── 12
                       21-23-09.303.bytes
                       21-23-09.801.bytes
                       21-23-10.300.bytes
                       21-23-10.834.bytes

```


### Unity client

Client executable location "_Executables\Unity Build\client_unity.exe"
Configuration file for client "_Executables\Unity Build\config.ini"

**It is possible to launch multiple unity clients at the same time.**

Configuration file contains settings for camera controller, server, and tracking manager. 
Explanation for some of them are below.

```
[camera controller configs]
move_speed=4                    : Camera move / panning speed using keyboards.
mouse_look_sensitivity=4        
zoom_speed=40                   : Zoom amount on mouse scroll.
move_smoothness=10
mouse_look_smoothness=30
invertY=false                   : Flip Y axis. Good for flight stick :p
use_right_click_look=false      : Hold right mouse button to look around - like we do in Unity's editor view.

[network configs]
server_url=tcp://127.0.0.1      : Listening server url
server_port=6120                : Listening server port
```

This last 3 settings are important for controlling the "Throughput vs Speed" curve. The framerates / tick rates in a game varies from game to game as well as machine to machine. The frame speed (Time.deltaTime) for a lightweight game can be ~0.003ms (~300 fps). So if we send data on per frame basis that would ~300 calls per second to server, and it's a serious load on server from just a single agent, where server is ready to listen from multiple agent at the same time.

```
[tracking manager configs]
tracking_mode=BY_NUMBER         : Two modes: BY_NUMBER | BY_TIME    
data_collection_time=1          : For BY_TIME Mode: it will collect data for x second(s) before sending them to the server.
data_collection_number=100      : For BY_NUMBER Mode: it will collect x number of data before sending them to the server.
```


## Given tasks status


* Implement a first person camera controller: **COMPLETED**
* Implement a tcp socket protocol between the Unity application and a C++ application using FlatBuffers and nng: **COMPLETED**
* Send a log of the player's actions from the Unity executable to a C++ program: **COMPLETED**
* Serialize the logs received by the C++ application into a log file: **COMPLETED**
* Extras:
    - Install Conan package manager and add a test executable using either Google Test or Catch2 testing frameworks: [Google Test Framework] **COMPLETED** 
    - Allow the C++ logger and the Unity application to be configurable using config files: **COMPLETED**


## Other design considerations

#### Why do we want to use flat buffers?
Suppose we are planning to send other kinds of messages to the logger to record the messages from different simulation agents, how would you design a message system to handle this elegantly?

A: One of the crucial reasons to use flat buffers is that the end size of the created object is very small. It creates a flat binary buffer which is significantly small in size compare to other popular serialization library (e.g. JSON of course.) Protocol buffer is a good contender but it needs unpacking / parsing of object, which flatbuffers does not need. And, finally, flexibility - we can change data structure without breaking the compatibility.

So, if we want to add support for other kinds of data from other agents it is possible without restructuring the whole thing because of flatbuffer flexibility. One of the solutions can be, create some generic templates to handle base flatbuffers schema. For example, in this project from the client side PacketBuild is a abstract class and TrackablePacketBuilder is derived from this class. On the server side, FBObjectParser is a generic template class and CameraLogParser is derived from this. Whenever we want to add a new thing we can use these templates to scale our project.


#### How will you design the C++ logger to listen to multiple publishing sockets?
Suppose we are planning to send other kinds of messages to the logger to record the actions of different simulation agents each running on their own process, how would you design a system to handle this requirement?

A: NNG's pull pipeline pattern supports listening from multiple publishing sockets. And, to support diversity, we can assign "UID" and "Type" for each agent. We don't need to unpack flatbuffers binary, so checking what type of agent is sending data would be fairly fast and simple. Depending on types and UID we will send that binary to designated handlers.



## Issue

C++ executables might give dll missing error because of missing VCRedist (Microsoft Visual C++ 2010 Redistributable Package). In that case you have to download and install the missing VCRedist.



**Thank you.**